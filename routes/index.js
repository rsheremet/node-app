var express = require('express');
var router = express.Router();
var os = require("os");


/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('index', {
		title: 'Express',
		category: 'Node Server',
		hostname: os.hostname(),
		networkInterfaces: JSON.stringify(os.networkInterfaces())
	});
});

module.exports = router;
