var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
	res.json({
		users: [
			{
				id: 1,
				profile:{
					name: "Robert",
					lname:"Green"
				}
			},
			{
				id: 2,
				profile:{
					name: "Dave",
					lname:"Black"
				}
			},
			{
				id: 3,
				profile:{
					name: "Mark",
					lname:"Yellow"
				}
			}

		]
	});
});

module.exports = router;
